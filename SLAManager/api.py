# Copyright 2015 SPECS Project - CeRICT

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#    http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# @author  Umberto Villano villano@unisannio.it
# @author  Antonio Punzo antonio.punzo@studenti.unisannio.it


import requests
from lxml import etree
from datetime import datetime
import time

URL = 'http://localhost:8080/sla_manager/cloud-sla/slas/'


def add_sla(data):
    headers = {'Content-type': 'text/xml'}
    r = requests.post(URL, headers=headers, data=data)
    if r.status_code != 201:
        raise Exception
    else:
        return r.content


def delete_sla(id):
    r = requests.delete(URL + str(id))
    if r.status_code != 200:
        raise Exception
    else:
        return r.content


def get_slas():
    r = requests.get(URL)
    root = etree.fromstring(r.content)
    SLAs = []
    for child in root:
        SLAs.append(SLA(child.attrib['id']))

    return SLAs


def get_sla(id):
    r = requests.get(URL + str(id))
    if r.status_code != 200:
        raise Exception
    else:
        root = etree.fromstring(r.content)
        return etree.tostring(root, pretty_print=True)


def get_sla_status(id):
    r = requests.get(URL + str(id) + '/status')
    if r.status_code != 200:
        raise Exception
    else:
        return r.content


def get_sla_customer(id):
    r = requests.get(URL + str(id) + '/customer')
    if r.status_code != 200:
        raise Exception
    else:
        return r.content


def update_sla(id, data):
    headers = {'Content-type': 'text/xml'}
    r = requests.put(URL + str(id), headers=headers, data=data)
    if r.status_code != 200:
        raise Exception
    else:
        return r.content


def user_sign_sla(id):
    r = requests.post(URL + str(id) + '/userSign')
    if r.status_code != 204:
        raise Exception
    else:
        return r.content


def terminate_sla(id):
    r = requests.post(URL + str(id) + '/terminate')
    if r.status_code != 204:
        raise Exception
    else:
        return r.content


def get_annotations(id):
    r = requests.get(URL + str(id) + '/annotations')
    if r.status_code != 200:
        raise Exception
    else:
        root = etree.fromstring(r.content)
        return etree.tostring(root, pretty_print=True)



def annotate_sla(id, ann_id, description):
    timestamp = datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    data = {'id': ann_id, 'description': description, 'timestamp': timestamp}
    r = requests.post(URL + str(id) + '/annotations', headers=headers, data=data)
    if r.status_code != 204:
        raise Exception
    else:
        return True


class SLA:
    def __init__(self, id):
        self.id = id
        self.status = get_sla_status(id)
        self.customer = get_sla_customer(id)
