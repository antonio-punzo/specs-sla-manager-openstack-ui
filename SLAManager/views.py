# Copyright 2015 SPECS Project - CeRICT

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#    http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# @author  Umberto Villano villano@unisannio.it
# @author  Antonio Punzo antonio.punzo@studenti.unisannio.it

from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse_lazy
import api
from tables import SLAsTable
from horizon import tables
from horizon import forms
from horizon import exceptions
from django.core.urlresolvers import reverse


from openstack_dashboard.dashboards.SPECS.SLAManager \
    import forms as project_forms


class IndexView(tables.DataTableView):
    template_name = 'SPECS/SLAManager/index.html'
    table_class = SLAsTable

    def get_data(self):
        return api.get_slas()


class AddSlaView(forms.ModalFormView):
    form_class = project_forms.AddSla
    modal_id = 'add_sla_modal'
    modal_header = _('Add SLA')
    template_name = 'SPECS/SLAManager/add_sla.html'
    success_url = reverse_lazy('horizon:SPECS:SLAManager:index')
    page_title = _('Add SLA')
    submit_label = _('Add')
    submit_url = reverse_lazy('horizon:SPECS:SLAManager:add_sla')


class UpdateSlaView(forms.ModalFormView):
    form_class = project_forms.UpdateSla
    modal_id = 'update_sla_modal'
    modal_header = _('Update SLA')
    template_name = 'SPECS/SLAManager/update.html'
    success_url = reverse_lazy('horizon:SPECS:SLAManager:index')
    page_title = _('Update SLA')
    submit_label = _('Update')
    submit_url = 'horizon:SPECS:SLAManager:update'


    def _get_sla(self):
        try:
            return api.get_sla(self.kwargs['id'])
        except Exception:
            exceptions.handle(self.request,
                              _('Unable to retrieve SLA.'))

    def get_initial(self):
        return {'id': self.kwargs['id'], 'sla':self._get_sla()}

    def get_context_data(self, **kwargs):
        context = super(UpdateSlaView, self).get_context_data(**kwargs)
        id = self.kwargs['id']
        context['id'] = id
        context['sla'] = self._get_sla()
        context['submit_url'] = reverse(self.submit_url, args=[id])
        return context


class AnnotateSlaView(forms.ModalFormView):
    form_class = project_forms.AnnotateSla
    modal_id = 'annotate_sla_modal'
    modal_header = _('Annotate SLA')
    template_name = 'SPECS/SLAManager/annotate.html'
    success_url = reverse_lazy('horizon:SPECS:SLAManager:index')
    page_title = _('Annotate SLA')
    submit_label = _('Annotate')
    submit_url = 'horizon:SPECS:SLAManager:annotate'

    def get_initial(self):
        return {'id': self.kwargs['id']}

    def get_context_data(self, **kwargs):
        context = super(AnnotateSlaView, self).get_context_data(**kwargs)
        id = self.kwargs['id']
        context['id'] = id
        context['submit_url'] = reverse(self.submit_url, args=[id])
        return context


class GetAnnotationsView(forms.ModalFormView):
    form_class = project_forms.GetAnnotations
    modal_id = 'get_annotations_modal'
    modal_header = _('Get Annotations')
    template_name = 'SPECS/SLAManager/annotations.html'
    success_url = reverse_lazy('horizon:SPECS:SLAManager:index')
    page_title = _('Get Annotations')
    submit_label = _('Add Annotation')
    submit_url = 'horizon:SPECS:SLAManager:annotations'


    def _get_annotations(self):
        try:
            return api.get_annotations(self.kwargs['id'])
        except Exception:
            exceptions.handle(self.request,
                              _('Unable to retrieve annotations.'))


    def get_initial(self):
        return {'id': self.kwargs['id'], 'annotations': self._get_annotations()}

    def get_context_data(self, **kwargs):
        context = super(GetAnnotationsView, self).get_context_data(**kwargs)
        id = self.kwargs['id']
        context['sla_id'] = id
        context['annotations'] = self._get_annotations()
        return context
