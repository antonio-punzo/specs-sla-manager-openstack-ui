# Copyright 2015 SPECS Project - CeRICT

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#    http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# @author  Umberto Villano villano@unisannio.it
# @author  Antonio Punzo antonio.punzo@studenti.unisannio.it

from django.utils.translation import ugettext_lazy as _

from horizon import tables
from horizon import exceptions

import api


class DeleteTableData(tables.DeleteAction):
    data_type_singular = _('SLA')
    data_type_plural = _('SLAs')

    def delete(self, request, obj_id):
        api.delete_sla(obj_id)


class AddTableData(tables.LinkAction):
    name = 'add'
    verbose_name = _('Add SLA')
    url = 'horizon:SPECS:SLAManager:add_sla'
    classes = ('ajax-modal',)


class UpdateTableData(tables.LinkAction):
    name = 'update'
    verbose_name = _('Update SLA')
    url = 'horizon:SPECS:SLAManager:update'
    classes = ('ajax-modal',)


class GetAnnotationsSla(tables.LinkAction):
    name = 'annotations'
    verbose_name = _('Get Annotations')
    url = 'horizon:SPECS:SLAManager:annotations'
    classes = ('ajax-modal',)


class AnnotateTableData(tables.LinkAction):
    name = 'annotate'
    verbose_name = _('Annotate SLA')
    url = 'horizon:SPECS:SLAManager:annotate'
    classes = ('ajax-modal',)


class UserSignSla(tables.Action):
    name = 'user_sign'
    verbose_name = _("User Sign")
    requires_input = False

    def allowed(self, request, datum):
        return datum.status == 'PENDING'

    def single(self, data_table, request, object_id):
        try:
            return api.user_sign_sla(object_id)
        except Exception:
            exceptions.handle(request,
                              _('Unable to sign SLA.'))


class TerminateSla(tables.Action):
    name = 'terminate'
    verbose_name = _("Terminate")
    requires_input = False

    def allowed(self, request, datum):
        return datum.status == 'OBSERVED' or datum.status == 'PROACTIVE_REDRESSING' \
               or datum.status == 'REMEDIATING' or datum.status == 'NEGOTIATING' \
               or datum.status == 'RENEGOTIATING'

    def single(self, data_table, request, object_id):
        try:
            return api.terminate_sla(object_id)
        except Exception:
            exceptions.handle(request,
                              _('Unable to terminate SLA.'))


class UpdateRow(tables.Row):
    ajax = True

    def get_data(self, request, id):
        return api.SLA(id)


class SLAsTable(tables.DataTable):
    id = tables.Column('id', verbose_name=_('ID'))
    status = tables.Column('status', verbose_name='Status')
    customer = tables.Column('customer', verbose_name='Customer')

    class Meta:
        name = 'SLAs'
        verbose_name = _('SLAs')
        status_columns = ['status']
        row_class = UpdateRow
        table_actions = (AddTableData, DeleteTableData,)
        row_actions = (UpdateTableData, UserSignSla, TerminateSla, GetAnnotationsSla,AnnotateTableData,)
